const express = require("express");
const cors = require("cors");
// const passport = require("passport")

const app = express();
const corsOption = {
    origin: 'http://localhost:3000',
    credentials: true
};


app.use(cors(corsOption));
// app.get("/",cors(),(req,res, next)=>{
//     res.send("I'm working...!")
// })
app.use(
  "*",
  cors({
    origin: "http://localhost:3000",
    //credentials: ''
  })
);
app.get("/", cors(), (req, res, next) => {
  res.send("I'm working...!");
});

app.use(express.json());
// app.use(passport.initialize())
// app.use(passport.session())
app.use("/api/user", require("./routes/user"));

app.use("/api/todo", require("./routes/todo"));
app.listen(5000, () => {
  console.log("listening on port 5000!");
});
