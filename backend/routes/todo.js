const router = require("express").Router();
const { PrismaClient, Prisma } = require("@prisma/client")

const{ user, todo } = new PrismaClient()


// GET ALL Todo ->> DONE
router.get("/allData", async (req,res) => {
    const all_posts = await todo.findMany({
        select:{
            titlename: true,
            list: true,
            todo_id: true,
            user_id: true,
            color: true,
            dateline: true
        }
    });

    res.json(all_posts);
})

// GET USER'S TODO
router.get('/user_allTodo/:id',async(req,res)=>{
    const userid=Number.parseInt(req.params.id);

    const allTodo = await todo.findMany({
        where:{
            OR:{
                user_id: userid,
                },
            NOT:{
                dateline: null
            }
        },
        //select:{ dateline:true, todo_id:true}
    });


    var now = new Date();
    date_line = Object.values(allTodo) 
    let duedate_pass = []
    let dateline_list = []
    for (let i = 0; i <date_line.length;i++){
        let dateline1 = date_line[i].dateline
        dateline_list.push(dateline1)
        if (now.getTime() > dateline1.getTime()){ // now มัน update กว่า = เลยแล้ว
            duedate_pass.push(
                {
                    "todo_id": date_line[i].todo_id,
                    "titlename": date_line[i].titlename,
                    "list": date_line[i].list,
                    "color": date_line[i].color,
                    "duedate": dateline1
                }
            )}
    }
   //res.json({date_line,dateline_list,duedate_pass})
   res.json({allTodo,duedate_pass})

//    const pass_posts = await todo.findMany({
  //      where:{
  //          todoid : duedate_pass.todoid
  //      },
  //      select:{
   //         titlename: true,
   //         list: true,
   //         todo_id: true,
   //         user_id: true,
   //         color: true,
   //         dateline: true
   //     }
    //});
    //res.json(pass_posts)
    
})




// Autocreate ->> DONE
router.post('/signin/:id', async (req,res)=>{
    const userid= Number.parseInt(req.params.id);
    const All = await todo.create({
        data:{
            titlename: "All",
            user_id: userid,
            color: "#B9472C"
        },
    })
    
    const List1 = await todo.create({
        data:{
            titlename : "List 1",
            user_id:   userid,
            color  :  "#AEADA8"
        },
    })
    const List2 = await todo.create({
        data:{
            titlename : "List 2",
            user_id:   userid,
            color  :  "#A2D6D2"
        },
    })
    res.json({msg: "already create"})
})

// GET USER'S LIST ->> DONE
router.get("/:id", async (req,res) => {
    const userid= Number.parseInt(req.params.id);
    //const {titlename}= req.body;
    let posts = await todo.findMany({
        where:{
            OR:{
                user_id: userid,
                //titlename: titlename
                },
            NOT:{
                list: null
            }
        },
        select:{
            todo_id:true,
            titlename:true,
            list: true,
            color: true,
           // dateline: true
        }
    });
    res.json(posts)
})

// ADD NEW TODOLIST ->> DONE
router.post('/addNewTodo/:id', async (req,res)=>{
    const userid= Number.parseInt(req.params.id);
    const {titlename , list, color} = req.body;

    const obj_todo = await todo.findMany(
        {where:{
            OR:{
                user_id: userid,
                titlename: titlename
                },
            NOT:{
                list: null
            }
        },
        select:{
            list:true
        }}
    )
    checklist = Object.values(obj_todo)
    checklist.forEach(element => {
        if( element == list){ return res.status(400).send({msg: "list already exist"})}
    });
    const newList = await todo.create({
        data:{
            titlename : titlename,
            list : list,
            user_id:   userid,
            color  :  color
        }
    })
    const allList = await todo.findMany({
        where:{
            OR:{
                user_id: userid,
                titlename: titlename
                },
            NOT:{
                list: null
            }
        },
        select:{
            todo_id:true,
            list:true,
            dateline: true
        }
    })
    res.json(allList)
})


// CREATE NEW TITLENAME ->> DONE
router.post("/addTitlename/:id", async(req,res)=>{
    const userid= Number.parseInt(req.params.id);
    const {titlename , color} = req.body;

    const createTitlename = await todo.create({
        data:{
            titlename : titlename,
            list : null,
            user_id:   userid,
            color  :  color
            },
        select:{
            titlename: true,
            color: true
        }
    })
    res.json(createTitlename)
})

// DELETE TitleList ->> DONE
router.delete("/delete_todoList/:id",async(req,res)=>{
    const userid= Number.parseInt(req.params.id);
    const {titlename} = req.body;
    //console.log("....")
    const deleteTitlename =await todo.deleteMany({
        where:{
            user_id:userid,
            titlename: titlename,
        },
        }
    )

   res.json(deleteTitlename);
})


// DELETE TODO ->> DONE
router.delete("/delete_todo", async(req,res)=>{
//  const userid= Number.parseInt(req.params.id);
    const {todo_id} =  req.body;
    //res.json(todo_id)
    const delete_todo = await todo.delete({
        where:{
            todo_id: todo_id
        },
    })
    res.json(delete_todo)
    //res.sendStatus(204).json({"msg":"delete Finsh"});
})


// CHANGE COLOR ->> DONE
router.patch("/changeColor/:id", async(req,res)=>{
    const userid= Number.parseInt(req.params.id);
    const { todo_id,newcolor } = req.body;
    //const todoid = parseInt(todo_id)

    const newTitlename = await todo.findFirst({
        where:{
            user_id: userid,
            color: newcolor
        }
    })
    const changeColor = await todo.update({
       data:{
            color: newcolor,
            titlename: newTitlename.titlename
        }, 
        where:{
            todo_id: todo_id
        }
    })
    res.json(changeColor)
})


// EDIT TODO ->> DONE
router.patch('/editTodolist/:id', async(req,res)=>{
    const userid = Number.parseInt(req.params.id)
    const {todo_id, newList} = req.body;
    const todoid = Number.parseInt(todo_id)
    const editedList = await todo.update({
        data:{
            list:newList
        },
        where:{
            todo_id: todoid
        }
    })
    res.json(editedList)
}) 



// ADD DUE DATE ->> DONE
let edit_time = (abb, hour)=>{

    if(abb === "AM") {
        let h = parseInt(hour)+7
        return String(h);}
    
    else{
        let n_hour = parseInt(hour)+12+7;
        n_hour = String(n_hour)    
        return n_hour;
    }
};
let dateToString =(day,month,year,hour,min)=>{
    return year+"-"+month+"-"+day+"-"+hour+"-"+min+"-0"
    
}

router.patch('/addDueDate', async(req,res)=>{
    const { todo_id, day,month,year,hour,min,abb}=req.body;
    const todoid = Number.parseInt(todo_id)

    const new_hour = edit_time(abb,hour);

   // let n_hour = parseInt(new_hour)+7;


    string_date = year.concat("-",month.concat("-",day))
    string_date = string_date.concat(" ",new_hour.concat(":",min))//.concat(":00")
    duedate = new Date(string_date)
    //d = new Date(year, month, day, hour,min,0)
    //date = d.toJSON()
    //res.json({duedate})

    const add_duedate = await todo.update({
        data:{
            dateline: duedate
        },
        where:{
            todo_id:todoid
        },
        select:{
            todo_id:true,
            list:true,
            dateline: true
        }
    });
    res.json(add_duedate)
})


module.exports=router