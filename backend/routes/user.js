const router = require("express").Router();
const { PrismaClient } = require("@prisma/client")

const{ user } = new PrismaClient()


router.get('/all',async(req,res) => {
    const users = await  user.findMany({
        select: {
            //user_id   :true,
            username :true,
            //password :true,
            //firstname :true,
           // lastname  :true,
           // email     :true,
            
           // profile:true,
        }
    });
    
    //const alluser = JSON.parse(users);
    let name = []
    for (let i = 0; i <users.length;i++){
        let value = users[i].username
        name.push(value)
    }
    
    //res.json({Name: name, Value :value, I: j})
    res.json({typr: typeof users, users, list:name})
})

// GET USER ->> DONE
router.get('/login',async(req,res) => {
    const users = await  user.findMany({
        select: {
            user_id: true,
            username: true,
            password: true
        }
    });
    res.json(users)
})


// NEW USER ->> DONE
router.post('/signin',async (req,res) =>{
    const {username,password,firstname,lastname,email}= req.body;

    const userRegist = await user.findUnique({
        where:{
            username : username
        },
        select:{
            username: true
        }
    });
    if(userRegist){
        return res.status(400).json({
            msg: "user already exists"
        })
    }


    const newUser = await user.create({
        data:{
            username: username,
            password: password,
            firstname: firstname,
            lastname: lastname,
            email: email,
            profile:"defaultProfile.png"         
        },
        select: {
            username: true,
            user_id: true
        }
    })
    
    res.json(newUser)
})


// // EDITUSERNAME & ICON PROFILE
// router.patch('/changeProfile/:id', async(req,res)=>{
//     const userid = Number.parseInt(req.params.id)
//     const {profile, newUsername} = req.body;
//     if( newUsername !== null){
//         const allUserName = await user.findMany({
//             select:{
//                 username: true
//             }
//         })
//         const all_username = Object.values(allUserName)
//         all_username.forEach(element => {
//             if( element == newUsername){ 
//                 return res.status(400).send({msg: "This username already exit, please enter new username."})
//             }
//         });
//         const change_profile = await user.update({
//         data:{
//             profile: profile,
//             username: username
//         },
//         where:{
//             user_id: userid
//         }
//     })
//     }else{
//         const change_profile = await user.update({
//             data:{
//                 profile: profile
//             },
//             where:{
//                 user_id: userid
//             }
//         })
//     }
//     res.json(change_profile)
// }) 

// CHANGE PROFILE ->> DONE
router.patch('/changeProfile/:id', async(req,res)=>{
    const userid=Number.parseInt(req.params.id);
    const {newprofile}= req.body;
    //console.log("5555555555");
    const change_profile = await user.update({
        
        where:{
            user_id: userid
        },
        data:{
            profile : newprofile
        },

    });
    res.json({msg: "Profile already change"});
})

// CHANGE USERNAME ->> DONE
router.patch('/changeUsername/:id', async(req,res)=>{
    const userid = Number.parseInt(req.params.id);
    const {newUsername}= req.body;

    const allUserName = await user.findMany({
        select:{
            username: true
        }
    });
    let users = []
    for (let i = 0; i <allUserName.length;i++){
        let value = allUserName[i].username
        users.push(value)
    }
    if( users.includes(newUsername) == true) {
        return res.status(400).send({msg: "This username already exit, please enter new username."})
    }


    const change_username = await user.update({
        
        where:{
            user_id: userid
        },
        data:{
            username: newUsername
        },
 //       select:{
  //          profile:true,
 //          username:true,
  //          password:true
  //      }
    }) ;
    
    res.json(change_username)
})



// CHANGE PASSWORD ->> DONE
router.patch('/changePassword', async(req,res)=>{
    const {username, password} = req.body;
    
    // data.list = Object.values(obj)
    const change_password = await user.update({
        where: {
            username
        },
        data: {
            password
        },
    })
   //     select:{
    //        user_id: true,
    //        username: true,
    //        password: true
    //    }
    ;
    res.json({"msg":"already change password"})
})




module.exports=router