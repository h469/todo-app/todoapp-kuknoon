/*
  Warnings:

  - You are about to alter the column `titlename` on the `todo` table. The data in that column could be lost. The data in that column will be cast from `VarChar(191)` to `VarChar(50)`.
  - You are about to alter the column `color` on the `todo` table. The data in that column could be lost. The data in that column will be cast from `VarChar(191)` to `VarChar(50)`.
  - You are about to alter the column `firstname` on the `user` table. The data in that column could be lost. The data in that column will be cast from `VarChar(191)` to `VarChar(50)`.
  - You are about to alter the column `lastname` on the `user` table. The data in that column could be lost. The data in that column will be cast from `VarChar(191)` to `VarChar(50)`.

*/
-- DropForeignKey
ALTER TABLE `todo` DROP FOREIGN KEY `TitleName_username_fkey`;

-- DropIndex
DROP INDEX `todo_user_id_titlename_key` ON `todo`;

-- DropIndex
DROP INDEX `User_firstname_key` ON `user`;

-- DropIndex
DROP INDEX `User_lastname_key` ON `user`;

-- DropIndex
DROP INDEX `User_password_key` ON `user`;

-- AlterTable
ALTER TABLE `todo` MODIFY `titlename` VARCHAR(50) NOT NULL,
    MODIFY `list` VARCHAR(100) NULL,
    MODIFY `color` VARCHAR(50) NOT NULL;

-- AlterTable
ALTER TABLE `user` MODIFY `firstname` VARCHAR(50) NOT NULL,
    MODIFY `lastname` VARCHAR(50) NOT NULL;

-- AddForeignKey
ALTER TABLE `todo` ADD CONSTRAINT `todo_user_id_fkey` FOREIGN KEY (`user_id`) REFERENCES `user`(`user_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
