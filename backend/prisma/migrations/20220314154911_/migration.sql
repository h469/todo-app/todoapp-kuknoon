/*
  Warnings:

  - You are about to alter the column `dateline` on the `todo` table. The data in that column could be lost. The data in that column will be cast from `VarChar(50)` to `VarChar(10)`.

*/
-- AlterTable
ALTER TABLE `todo` MODIFY `dateline` VARCHAR(10) NOT NULL;
