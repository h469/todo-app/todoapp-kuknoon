/*
  Warnings:

  - Added the required column `dateline` to the `todo` table without a default value. This is not possible if the table is not empty.
  - Added the required column `profile` to the `user` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE `todo` ADD COLUMN `dateline` VARCHAR(50) NOT NULL;

-- AlterTable
ALTER TABLE `user` ADD COLUMN `profile` VARCHAR(300) NOT NULL;
