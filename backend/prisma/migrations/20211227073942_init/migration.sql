/*
  Warnings:

  - You are about to drop the `titlename` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `todolist` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE `titlename` DROP FOREIGN KEY `TitleName_username_fkey`;

-- DropForeignKey
ALTER TABLE `todolist` DROP FOREIGN KEY `TodoList_titlename_fkey`;

-- DropTable
DROP TABLE `titlename`;

-- DropTable
DROP TABLE `todolist`;

-- CreateTable
CREATE TABLE `todo` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `username` VARCHAR(191) NOT NULL,
    `titlename` VARCHAR(191) NOT NULL,
    `list` VARCHAR(100) NOT NULL,

    UNIQUE INDEX `todo_username_titlename_key`(`username`, `titlename`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- AddForeignKey
ALTER TABLE `todo` ADD CONSTRAINT `TitleName_username_fkey` FOREIGN KEY (`username`) REFERENCES `user`(`username`) ON DELETE RESTRICT ON UPDATE CASCADE;
