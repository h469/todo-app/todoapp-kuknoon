/*
  Warnings:

  - You are about to alter the column `color` on the `todo` table. The data in that column could be lost. The data in that column will be cast from `VarChar(50)` to `VarChar(10)`.
  - You are about to alter the column `username` on the `user` table. The data in that column could be lost. The data in that column will be cast from `VarChar(100)` to `VarChar(50)`.

*/
-- AlterTable
ALTER TABLE `todo` MODIFY `color` VARCHAR(10) NOT NULL;

-- AlterTable
ALTER TABLE `user` MODIFY `username` VARCHAR(50) NOT NULL;
