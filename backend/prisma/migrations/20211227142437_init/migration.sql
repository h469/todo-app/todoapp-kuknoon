/*
  Warnings:

  - Added the required column `color` to the `todo` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE `todo` ADD COLUMN `color` VARCHAR(191) NOT NULL;
