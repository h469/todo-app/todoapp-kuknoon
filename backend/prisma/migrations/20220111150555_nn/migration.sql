/*
  Warnings:

  - A unique constraint covering the columns `[titlename,list]` on the table `todo` will be added. If there are existing duplicate values, this will fail.

*/
-- CreateIndex
CREATE UNIQUE INDEX `todo_titlename_list_key` ON `todo`(`titlename`, `list`);
