/*
  Warnings:

  - You are about to alter the column `dateline` on the `todo` table. The data in that column could be lost. The data in that column will be cast from `VarChar(10)` to `DateTime(3)`.

*/
-- AlterTable
ALTER TABLE `todo` MODIFY `dateline` DATETIME(3) NOT NULL;
