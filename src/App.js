import { Route, Routes } from "react-router-dom";

import RegisterPage from "./pages/input/register";
import LoginPage from "./pages/input/login";
import MainPage from "./pages/main";
import ResetPassPage from "./pages/input/resetpass";
import "./App.css";

import { GlobalProvider } from "./context/globalstate";

function App() {
  return (
    <GlobalProvider>
      <Routes>
        <Route path="/" element={<RegisterPage />} exact />
        <Route path="/login" element={<LoginPage />} />
        <Route path="/main" element={<MainPage />} />
        <Route path="/resetpass" element={<ResetPassPage />} />
      </Routes>
    </GlobalProvider>
  );
}

export default App;
