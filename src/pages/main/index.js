import { useState, useEffect, useContext } from "react";
import axios from "axios";
import { UserContext } from "../input/login";

import Navbar from "../../components/todos/navbar/index.js";
import Content from "../../components/todos/content/index.js";
import Background from "../../components/background/index.js";
import styles from "./main.module.css"

import { GlobalContext } from "../../context/globalstate";

// {
//   titlename: "All",
//   list: "walk the dog",
//   todo_id: 1,
//   user_id: 1,
//   color: "#B9472C",
// },

const todo = [
  {
    titlename: "All",
    list: "walk the dog",
    todo_id: 1,
    user_id: 1,
    color: "#B9472C",
  },
  {
    titlename: "All",
    list: null,
    todo_id: 2,
    user_id: 1,
    color: "#B9472C",
  },
  {
    titlename: "All",
    list: "do sth",
    todo_id: 3,
    user_id: 2,
    color: "#B9472C",
  },
  {
    titlename: "All",
    list: "sleep early",
    todo_id: 4,
    user_id: 1,
    color: "#B9472C",
  },
  {
    titlename: "List 1",
    list: null,
    todo_id: 5,
    user_id: 1,
    color: "#AEADA8",
  },
  {
    titlename: "List 1",
    list: "do homework",
    todo_id: 6,
    user_id: 1,
    color: "#AEADA8",
  },
  {
    titlename: "All",
    list: "wash dish",
    todo_id: 7,
    user_id: 1,
    color: "#B9472C",
  },
  {
    titlename: "List 1",
    list: "reading",
    todo_id: 8,
    user_id: 1,
    color: "#AEADA8",
  },
  {
    titlename: "All",
    list: null,
    todo_id: 9,
    user_id: 2,
    color: "#B9472C",
  },
  {
    titlename: "All",
    list: "at swh",
    todo_id: 10,
    user_id: 2,
    color: "#B9472C",
  },
  {
    titlename: "List 1",
    list: null,
    todo_id: 11,
    user_id: 2,
    color: "#AEADA8",
  },
  {
    titlename: "List 1",
    list: "meet friend",
    todo_id: 12,
    user_id: 2,
    color: "#AEADA8",
  },
  {
    titlename: "Exercise",
    list: "sit up",
    todo_id: 13,
    user_id: 1,
    color: "#FE9500",
  },
];

function MainPage() {
  // const [todo, setTodo] = useState([]);
  // const { user } = useContext(UserContext);

  // console.log(user);
  const { user, setTodos, todos } = useContext(GlobalContext);

  useEffect(() => {
    axios
      .get(`${process.env.REACT_APP_HOST}/api/todo/${user.user_id}`)
      .then((result) => {
        const { data } = result;
        setTodos(data);
      });
  }, []);
  console.log(todos);

  return (
    <>
      <Background />
      <Navbar />
      {/* <div className={styles.taskitem}>
        <Content/>
      </div> */}
    </>
  );
}

export default MainPage;
