import styles from "./form.module.css";

import { useState } from "react";

function ResetPassPage() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [isSubmit, setIsSubmit] = useState(false);

  const usernameForm = (
    <form className={styles.form}>
      <input
        type="text"
        id="username"
        required
        placeholder="Username *"
        onChange={(event) => {
          setUsername(event.target.value);
        }}
      />
      <button className={styles.btn}>Submit</button>
    </form>
  );

  const passwordForm = (
    <form className={styles.form}>
      <input
        type="password"
        id="password"
        required
        placeholder="New Password *"
        onChange={(event) => {
          setPassword(event.target.value);
        }}
      />
      <input
        type="password"
        id="password"
        required
        placeholder="Comfirm Password *"
        onChange={(event) => {
          setPassword(event.target.value);
        }}
      />
      <button className={styles.btn}>Reset Password</button>
    </form>
  );

  return (
    <div className={styles.container}>
      <img className={styles.profile} src="tempProfile.png" alt="tempProfile" />
      <h2>Reset Password</h2>
      {isSubmit ? passwordForm : usernameForm}
    </div>
  );
}

export default ResetPassPage;
