import { useState, useEffect, useContext } from "react";
import { useNavigate, Link } from "react-router-dom";
import axios from "axios";

import styles from "./form.module.css";

import { GlobalContext } from "../../context/globalstate";

// const users = [
//   {
//     user_id: 1,
//     username: "kazuha00",
//     password: "123",
//     firstname: "Kazuha",
//     lastname: "Kaedehara",
//     email: "KK@mail.com",
//     todo: [],
//   },
//   {
//     user_id: 2,
//     username: "thomo01",
//     password: "0101",
//     firstname: "Thomodaji",
//     lastname: "nai",
//     email: "thn@mail.com",
//     todo: [],
//   },
// ];

// const UserContext = createContext("");

function LoginPage() {
  const [users, setUsers] = useState([]);
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [validateUser, setValidateUser] = useState(true);
  const { setUser, user } = useContext(GlobalContext);
  let nextPage = useNavigate();

  useEffect(() => {
    axios.get(`${process.env.REACT_APP_HOST}/api/user/login`).then((result) => {
      const { data } = result;
      setUsers(data);
    });
  }, []);
  // console.log(users);

  function LoginHandler(event) {
    event.preventDefault();
    let isMember = false;
    const loginData = {
      username: username,
      password: password,
      user_id: "",
    };
    users.forEach((user) => {
      if (
        user.username === loginData.username &&
        user.password === loginData.password
      ) {
        isMember = true;
        loginData.user_id = user.user_id;
      }
    });
    if (isMember) {
      setUser({ username: loginData.username, user_id: loginData.user_id });
      nextPage("/main");
    } else {
      setValidateUser(false);
      setUsername("");
      setPassword("");
    }
  }
  // console.log(user);

  return (
    <div className={styles.container}>
      <img className={styles.profile} src="tempProfile.png" alt="tempProfile" />
      <h2>Login</h2>
      <form onSubmit={LoginHandler} className={styles.form}>
        <input
          type="text"
          id="username"
          required
          placeholder="Username"
          onChange={(event) => {
            setUsername(event.target.value);
          }}
          value={username}
        />
        <input
          type="password"
          id="password"
          required
          placeholder="Password"
          onChange={(event) => {
            setPassword(event.target.value);
          }}
          value={password}
        />
        {!validateUser && <p>Wrong username or password, Please try again.</p>}
        <button className={styles.btn}>Login</button>
      </form>

      <span>
        <Link style={{ textDecorationLine: "none" }} to="/resetpass">
          Forgot password?
        </Link>
      </span>
    </div>
  );
}

export default LoginPage;
