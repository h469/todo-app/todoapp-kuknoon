import { useNavigate, Link } from "react-router-dom";
import { useState } from "react";
import axios from "axios";

import styles from "./form.module.css";

// import * as dotenv from 'dotenv'
// dotenv.config({path: 'path'})

function RegisterPage() {
  let nextPage = useNavigate();
  const [data, setData] = useState([]);

  const [registerdata, setRegisterData] = useState({
    firstname: "",
    lastname: "",
    username: "",
    email: "",
    password: "",
  });

  function registerChange(event) {
    const { id, value } = event.target;
    setRegisterData((prev) => ({
      ...prev,
      [id]: value,
    }));
  }

  function registerHandler(event) {
    event.preventDefault();
    axios
      .post(`${process.env.REACT_APP_HOST}/api/user/signin`, registerdata)
      .then((response) => console.log(response));
    nextPage("/login");
  }

  // function addRegisterDataHandler(data) {
  //   axios
  //     .post(`${process.env.REACT_APP_HOST}/api/user/signin`, data)
  //     .then((response) => setData(response.data));
  //   nextPage("/login");
  // }

  return (
    <div className={styles.container}>
      <img className={styles.profile} src="tempProfile.png" alt="tempProfile" />
      <h2>Register</h2>
      <form onSubmit={registerHandler} className={styles.form}>
        <div className={styles.pairInput}>
          <input
            type="text"
            id="firstname"
            required
            placeholder="First name *"
            onChange={registerChange}
            value={registerdata.firstname}
          />
          <input
            type="text"
            id="lastname"
            required
            placeholder="Last name *"
            onChange={registerChange}
            value={registerdata.lastname}
          />
        </div>
        <input
          type="text"
          id="username"
          required
          placeholder="Username *"
          onChange={registerChange}
          value={registerdata.username}
        />
        <input
          type="text"
          id="email"
          required
          placeholder="E-mail *"
          onChange={registerChange}
          value={registerdata.email}
        />
        <input
          type="password"
          id="password"
          required
          placeholder="Password *"
          onChange={registerChange}
          value={registerdata.password}
        />
        <button className={styles.btn}>Register</button>
        <span>
          Already a member? <Link to="/login">Login</Link>
        </span>
      </form>
    </div>
  );
}

export default RegisterPage;
