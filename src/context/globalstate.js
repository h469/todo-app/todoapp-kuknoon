import React, { createContext, useReducer } from "react";
import AppReducer from "./reducer";

const initialState = {
  todos: [],
  user_id: {},
};

export const GlobalContext = createContext(initialState);

export const GlobalProvider = ({ children }) => {
  const [state, dispatch] = useReducer(AppReducer, initialState);

  //Actions
  function deleteTodo(id) {
    dispatch({
      type: "DELETE_TODO",
      payload: id,
    });
  }

  function setUser(user) {
    dispatch({
      type: "SET_USER",
      payload: user,
    });
  }

  function addTodo(todo) {
    dispatch({
      type: "ADD_TODO",
      payload: todo,
    });
  }

  function setTodos(allTodo) {
    dispatch({
      type: "SET_TODOS",
      payload: allTodo,
    });
  }

  return (
    <GlobalContext.Provider
      value={{
        todos: state.todos,
        deleteTodo,
        addTodo,
        user: state.user,
        setUser,
        setTodos,
      }}
    >
      {children}
    </GlobalContext.Provider>
  );
};
