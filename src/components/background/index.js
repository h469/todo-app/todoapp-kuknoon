import styles from "./background.module.css";

function Background() {
  return (
    <>
      <div className={styles.whiteBackground} />
      <div className={styles.taskItemBackground} />
    </>
  );
}

export default Background;
