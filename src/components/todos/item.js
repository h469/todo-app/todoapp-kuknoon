import { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCircle } from "@fortawesome/free-solid-svg-icons";
import styles from "./content/content.module.css";

function TodoItem(props) {
  const value = props.value;
  const TODO_DATA = props.TODO_DATA;
  const [clickEditTodo, setClickEditTodo] = useState(false);
  const [editText, setEditText] = useState("");

  //ส่ง todo ที่แก้ไขแล้ว [Backend]
  const submitEditedTodoHandler = (event) => {
    // **** ส่ง todo_id, list
    event.preventDefault();
    console.log(editText);
    setClickEditTodo(false);
  };

  // ลบ todo [Backend]
  function deleteTodoHandler(todo_id) {
    // **** ส่ง todo_id ให้ backend
    // console.log(todo_id)
    let updateTodos = TODO_DATA.filter((todo) => {
      return todo.todo_id !== todo_id;
    });
    props.updateTodosHandler(updateTodos);
  }

  // เก็บ todo_id ไว้เอาไปใช้กับ set Click
  function editTodoHandler(todo_id) {
    const todo = TODO_DATA.find((todo) => todo.todo_id === todo_id);
    console.log(todo);
    setEditText(todo.list);
    setClickEditTodo(true);
  }

  return (
    <div className={styles.todo} id={value.todo_id}>
      <div className={styles.row}>
        <input
          type="checkbox"
          onChange={() => deleteTodoHandler(value.todo_id)}
        />

        {clickEditTodo ? (
          <form className={styles.form} onSubmit={submitEditedTodoHandler}>
            <input
              type="text"
              onChange={(event) => setEditText(event.target.value)}
              value={editText}
            />
          </form>
        ) : (
          <label onClick={() => editTodoHandler(value.todo_id)}>
            {value.list}
          </label>
        )}
      </div>

      <div style={{ color: value.color }} className={styles.row}>
        <FontAwesomeIcon icon={faCircle} />
      </div>
    </div>
  );
}

export default TodoItem;
