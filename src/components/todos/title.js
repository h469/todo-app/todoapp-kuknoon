import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCircle } from "@fortawesome/free-solid-svg-icons";

import styles from "./navbar/navbar.module.css";

function TodoTitle(props) {
  // console.log(props.TODO_DATA);

  return (
    <li
      id={props.titlename}
      onClick={() => props.taskListHandler(props.titlename)}
    >
      <div className={styles.row}>
        <div className={styles.icon} style={{ color: props.color }}>
          <FontAwesomeIcon icon={faCircle} />
        </div>

        <div className={styles.title}>{props.titlename} </div>

        <div className={styles.total} style={{ color: props.color }}>
          {props.allTodos.length}{" "}
        </div>
      </div>
    </li>
  );
}

export default TodoTitle;
