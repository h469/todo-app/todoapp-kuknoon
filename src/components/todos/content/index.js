import { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCircle } from "@fortawesome/free-solid-svg-icons";
import axios from "axios";

import styles from "./content.module.css";
import TodoItem from "../item";

function Content(props) {
  const TODO_DATA = props.allTodos;
  const [clickAddTodo, setClickAddTodo] = useState(false);
  const [count, setCount] = useState(1);
  const [newTodo, setnewTodo] = useState("");
  const [data, setData] = useState([]);

  //คลิกเปิดปิดฟอร์มตอน Add new to do
  function addNewTodoHandler() {
    setCount(count + 1);
    if (count % 2 !== 0) {
      setClickAddTodo(true);
    } else {
      setClickAddTodo(false);
    }
  }

  //เพิ่ม new to do [Backend]
  function submitNewTodoHandler(event) {
    event.preventDefault();
    setClickAddTodo(false);
    const todo = {
      titlename: props.titleData.titlename,
      list: newTodo,
      color: props.titleData.color,
    };
    axios
      .post(
        `${process.env.REACT_APP_HOST}/api/todo/addNewTodo/${props.user_id}`,
        todo
      )
      .then((response) => setData(response.data));
    const updateTodos = [...TODO_DATA].concat(todo);
    props.updateTodosHandler(updateTodos);
    console.log(data);
  }

  return (
    <div className={styles.container}>
      <div className={styles.item}>
        <h1>To-Do List!</h1>

        <div className={styles.row}>
          <button onClick={addNewTodoHandler} className={styles.btn}>
            + Add new to do
          </button>

          {clickAddTodo && (
            <form className={styles.todo} onSubmit={submitNewTodoHandler}>
              <div className={styles.row}>
                <input type="checkbox" />
                <input
                  type="text"
                  onChange={(event) => setnewTodo(event.target.value)}
                />
              </div>

              <div
                style={{ color: props.titleData.color }}
                className={styles.row}
              >
                <FontAwesomeIcon icon={faCircle} />
              </div>
            </form>
          )}
        </div>

        <div className={styles.row}>
          {TODO_DATA.map((value) => {
            return (
              <TodoItem
                value={value}
                TODO_DATA={TODO_DATA}
                updateTodosHandler={props.updateTodosHandler}
              />
            );
          })}
        </div>
      </div>
    </div>
  );
}

export default Content;
