import { useNavigate } from "react-router-dom";
import { useEffect, useState, useContext } from "react";

import styles from "./navbar.module.css";
import Background from "../../background";
import ColorForm from "../../colorpalette/colorpalette";
import TodoTitle from "../title";

import { GlobalContext } from "../../../context/globalstate.js";

const defaultTitles = [
  {
    titlename: "All",
    color: "#B9472C",
    total: 0,
  },
  {
    titlename: "List 1",
    color: "#AEADA8",
    total: 0,
  },
  {
    titlename: "List 2",
    color: "#A2D6D2",
    total: 0,
  },
];

function Navbar(props) {
  const RegisterPage = useNavigate();
  // const TODO_DATA = props.todo;

  const { user } = useContext(GlobalContext);

  console.log(user)

  // todos ของแต่ละ titlename
  // const [allTodos, setAllTodos] = useState(
  //   TODO_DATA.filter((todo) => {
  //     return todo.titlename === "All";
  //   })
  // );

  const [allTitles, setAllTitles] = useState(defaultTitles); // titles ทั้งหมด เป็น object
  // const [count, setCount] = useState(1);
  const [clickCreateNewList, setClickCreateNewList] = useState(false);
  const [color, setColor] = useState("");
  const [titlename, setTitlename] = useState("");
  const [titleData, setTitleData] = useState(defaultTitles[0]); //ชื่อ title ตอนคลิก ไว้กรอง todo

  // list เฉพาะชื่อของแต่ละ title
  const [titlenameList, setTitlenameList] = useState(
    defaultTitles.map((object) => {
      return object.titlename;
    })
  );

  function logoutHandler() {
    RegisterPage("/");
  }

  //หา list ทั้งหมดที่มีที่ไม่ซํ้า
  // TODO_DATA.forEach((todo) => {
  //   if (!titlenameList.includes(todo.titlename)) {
  //     allTitles.push({
  //       titlename: todo.titlename,
  //       color: todo.color,
  //       total: todo.total,
  //     });
  //     titlenameList.push(todo.titlename);
  //   }
  // });

  //สร้าง list ใหม่
  // function creatNewListHandler(event) {
  //   event.preventDefault();
  //   const newList = {
  //     user_id: props.user_id,
  //     titlename: titlename,
  //     color: color,
  //     total: 0,
  //   };
  //   const updateLists = [...allTitles].concat(newList);
  //   const updateTitlename = [...titlenameList].concat(titlename);
  //   setTitlenameList(updateTitlename);
  //   setAllTitles(updateLists);
  // }

  //กรอง todo ทั้งหมดของแต่ละ titlemname
  function taskListHandler(titlename) {
    // let todos = TODO_DATA.filter((todo) => {
    //   return titlename === todo.titlename;
    // });
    // const indexOfTitlename = titlenameList.indexOf(titlename);

    // setTitleData(allTitles[indexOfTitlename]);
    // setAllTodos(todos);
    // console.log(todos);
  }

  //update todo ที่มี
  // function updateTodosHandler(updateTodos) {
  //   // console.log(updateTodos)
  //   setAllTodos(updateTodos);
  // }

  // function clickCreateNewListHandler() {
  //   setCount(count + 1);
  //   if (count % 2 !== 0) {
  //     setClickCreateNewList(true);
  //   } else {
  //     setClickCreateNewList(false);
  //   }
  // }

  // allTitles.forEach((title) => {
  //   title.total = TODO_DATA.filter((todo) => {
  //     return todo.titlename === title.titlename;
  //   }).length;
  // });

  // console.log(allTitles);

  return (
    <div>
      <div className={styles.tasklist}>
        <div>
          <div className={styles.container}>
            <p>{user.username}</p>
          </div>
          <ul>
            {allTitles.map((value, key) => {
              return (
                <TodoTitle
                  total={value.total}
                  titlename={value.titlename}
                  color={value.color}
                  // TODO_DATA={TODO_DATA}
                  // allTodos={allTodos}
                  taskListHandler={taskListHandler}
                />
              );
            })}
            <div
              // onClick={clickCreateNewListHandler}
              className={styles.createNewList}
            >
              + Create a new list
            </div>
            {clickCreateNewList && (
              <form
                className={styles.createNewListForm}
                // onSubmit={creatNewListHandler}
              >
                <div>
                  <input
                    onChange={(e) => setTitlename(e.target.value)}
                    type="text"
                    placeholder="Name"
                  />
                </div>

                <ColorForm setColor={setColor} />
              </form>
            )}
          </ul>
        </div>
        <div className={styles.container}>
          <button className={styles.btn} onClick={logoutHandler}>
            logout
          </button>
        </div>
      </div>
      {/* <div className={styles.taskitem}>
        <Content
          user_id={props.user_id}
          titleData={titleData}
          allTodos={allTodos}
          updateTodosHandler={updateTodosHandler}
        />
      </div> */}
    </div>
  );
}

export default Navbar;
