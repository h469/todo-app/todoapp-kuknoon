import { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCircle } from "@fortawesome/free-solid-svg-icons";

import styles from "./colorpalette.module.css";

function ColorForm(props) {
  const colorList = [
    "#B9472C",
    "#AEADA8",
    "#A2D6D2",
    "#B65C7B",
    "#775370",
    "#FE9500",
  ];

  return (
    <div className={styles.container}>
      {colorList.map((color) => {
        return (
          <div className={styles.row} style={{ color: color }}>
            <span>
              <FontAwesomeIcon
                onClick={() => props.setColor(color)}
                key={color}
                icon={faCircle}
              />
            </span>
          </div>
        );
      })}
    </div>
  );
}

export default ColorForm;
